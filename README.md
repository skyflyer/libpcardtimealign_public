# libPcardTimeAlign

## About

This library manages time alignment in PCARD environment. Time alignment is the procedure of fixing errors in time stamps and sample counters in s2 files or in live measurements

## Alignment Methodology

### Fixed-frequency

A sampling frequency is given to the algorithm as a parameter. Then the data (pairs of (timestamp, counter) that are given as a custom class) are streamed in and output is collected that is time aligned: both timestamp and counter are modified. 

If the given frequency and the actual frequency differ a lot, then some hard to recover artifacts can occur: the length of each long pause (the threshold pause length is a parameter) is dilated or contracted to compensate for the accumulated difference in number of samples taken under the real and assumed frequencies.

PWP normal data stream is assumed (hardcoded for now), with 14 samples and 10-bit counter. The code can be easily extended to support different data streams (different counter bit-widths and number of samples per packet).
