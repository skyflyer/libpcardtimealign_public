package si.ijs.e6.pcard;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.FINE;

/**
 * The simplest real-time alignment algorithm (implemented very similarly to the s2-to-nekg converter).
 * It only keeps knowledge of one past timepoint, from which it infers the relative temporal position of
 * the next packet.
 *
 * Note: this method is fine-tuned to work with 10-bit counters with 14 sample increment!
 *
 * This should not be used with measurements performed on wearable gadgets with firmware < 1.0.2 (some issues
 * with those measurements are not resolved here)
 */
class TimeAlignSimpleRealTimeAlgorithm<TP extends TimestampPair> implements PcardTimeAlignment.TimeAlignAlgorithmInterface<TP> {
    private static final Logger logger = Logger.getLogger(PcardTimeAlignment.class.getName());
    private static final int COUNTER_INCREMENT = 14;
    static final long MIN_INTERVAL_FOR_UNMARKED_DISCONNECT = 1000000000L; // 3 seconds

    public static boolean DEBUG_OUTPUT = false;

    long counterOffset = 0;
    long prevCounter = PcardTimeAlignment.INVALID_COUNTER;
    int prevCounterOvf = 0;
    long prevExtendedCounter = 0;
    long prevTimestamp = PcardTimeAlignment.INVALID_TIMESTAMP;
    // when this variable is set, the newly registered packet will be assumed to not be related to the past
    boolean expectFirstPacket = true;
    /**
     * A constant sampling frequency is set here; this is actually a factor between counter and timestamp.
     * Example: sampling frequency is 128 Hz, counter then increases by 128 in 1 second;
     *          timestamps are in nanoseconds, they increase by 1e9 in one second;
     *          constantFs = counter/timestamp = 128 / 1e9 = 128e-9
     * TODO: if lower than 0 then ...
     */
    double constantFs = -1.0;
    double constantSamplingTime = -1.0;

    // reference timestamp is the timestamp at counter = 0; this is calculated from the first collection of datum
    // pairs and is left as is for subsequent collections, reset on disconnects
    long referenceTimestamp = PcardTimeAlignment.INVALID_TIMESTAMP;
    // count 'blocks', that is continuous streams of data between marked and unmarked disconnects
    // counter of aligned pairs (total global sum)
    long countOutputs = 0;
    // an archive of blocks (already processed)
    List<PcardTimeAlignment.BlockInfo> blocks = new ArrayList<>(100);
    // currently active block; pairs are still being added to it
    PcardTimeAlignment.BlockInfo activeBlock = new PcardTimeAlignment.BlockInfo();
    // first block that was not flushed to output yet
    int firstNotFlushedBlock = 0;
    boolean silentDisconnectDetection = true;

    void markBlockStart() {
        referenceTimestamp = PcardTimeAlignment.INVALID_TIMESTAMP;
    }

    boolean blockHasJustStarted() {
        return referenceTimestamp == PcardTimeAlignment.INVALID_TIMESTAMP;
    }

    /**
     * Extract the overflow component from 10-bit counter
     * This component can be used to estimating number of overflows between two consecutive counters.
     * <p>
     * Given two counters, c1 and c2, where c2 comes after c1 (in a later packet, that can be known for sure);
     * calculate ovf = get10BitCounterOverflowComponent(c2)-get10BitCounterOverflowComponent(c1);
     * if ovf == 0 then k*7 overflows have occurred (most likely k=0, therefore no overflows);
     * if ovf  > 0 then ovf + k*7 overflows have occurred (most likely k=0, therefore ovf overflows);
     * if ovf  < 0 then ovf + (k+1)*7 overflows have occurred (most likely k=0, therefore ovf+7 overflows);
     * Since ovf is a number between 0 and 6, there are max 7 overflows that can be measured with it before it
     * starts cycling as well. If sampling frequency is 125, then the ofv cycle will occur in 7*1024/125 seconds,
     * that is 57.3 seconds, which is far longer then the BLE timeout so should never happen. Therefore one can
     * safely assure that k==0 in the above if statements.
     * <p>
     * For example, three counters c1, c2, and c3:
     * c1 = 1012;
     * c2 = 2  (1012+14=1026, 1026 % 1024 = 2)         // single overflow
     * c3 = 10 (1012+221*14=4106; 4106 % 1024 = 10)    // four overflows (1012+, 2034+, 3070+, 4092+)
     * get10BitCounterOverflowComponent(c1) -> 4
     * get10BitCounterOverflowComponent(c2) -> 5
     * get10BitCounterOverflowComponent(c2) -> 1
     * (c2-c1+7) % 7 = 1   // therefore one overflow between 1012 and 2
     * (c3-c1+7) % 7 = 4   // therefore four overflow between 1012 and 10
     * // Note in java modulo operand can return negative numbers, therefore +7 is used in above calculations
     * <p>
     * Note: get10BitCounterOverflowComponent(n) == get10BitCounterOverflowComponent(n + k*1024)
     * Therefore, no need in recalculating overflow component when fixing counters by multiple of 1024
     *
     * @param raw10BitCounter The 10 bit counter as received from PCARD weareable gadget
     * @return a component that indicates number of overflows (7 possible numbers: integers >= 0 and <= 6)
     */
    private static int get10BitCounterOverflowComponent(int raw10BitCounter) {
        // first, get the modulo of the overflow; possible values are [13 11 9 7 5 3 1] for odd counters and
        // [13 11 9 7 5 3 1] for even counters
        int overflowModulated = (raw10BitCounter % 1024) % 14;
        // convert the overflowModulated to increasing number by 1 and unify for odd and even
        return 6 - (int) Math.floor(overflowModulated / 2);
    }

    /**
     * Helper function for calculating the time equivalence of a given timer difference
     *
     * @param ts  initial timestamp
     * @param cnt timer difference/increase, relative to the initial timestamp
     * @return the calculated timestamp
     */
    protected long offsetTimestampByCounterValue(long ts, long cnt) {
        return Math.round(ts + (cnt * constantSamplingTime));
    }

    @Override
    public void enableSilentDisconnectsDetection(boolean enabled) {
        // silent disconnects are present only on older hardware and are not detected yet
        silentDisconnectDetection = enabled;
    }

    @Override
    public PcardTimeAlignment.DataCollection<TP> alignData(PcardTimeAlignment.DataCollection<TP> input, PcardTimeAlignment.DataCollection<TP> cache) {
        // Create the output collection to collect the algorithm results
        PcardTimeAlignment.DataCollection<TP> output = new PcardTimeAlignment.DataCollection<>(input.size());
        // if a disconnect is MARKED, this variable will be set to true, initialize it to false
        boolean markedDisconnect = false;
        // if ANY disconnect is detected this var will turn true, init it to false
        boolean blockFinished = false;

        // check that at least one element is present, otherwise return the empty output
        if (input.size() < 1)
            return output;

        // process counters until the end of input collection or until an unmarked disconnect is detected
        int numProcessed = input.size();

        // expand counters and remove invalid counters, which are flagging the disconnections
        // Note: at least one element must be processed!
        for (int i = 0; i < input.size(); ++i) {
            TP p = input.get(i);
            // copy the data across caches (input -> output) then modify output in-place

            // TODO: refactor into first checking p, then adding it to output
            output.addDatumPoint(p);
            if (!processPacketCounter(output.get(i))) {
                // processPacketCounter returns false on disconnects
                blockFinished = true;

                // INVALID counter in the output marks registered disconnect
                // test for disconnect
                if (output.get(i).getCounter() == PcardTimeAlignment.INVALID_COUNTER) {
                    markedDisconnect = true;
                    output.removeLastDatum();
                    // processed are all elements up to disconnect mark and the mark itself (the mark should not
                    // be left in the output)
                    numProcessed = i + 1;
                } else {
                    // the alternative - an unregistered disconnect was found; the last processed pair from input
                    // will have to be re-processed
                    output.removeLastDatum();
                    // processed are all elements up to current one, which is a part of a new block
                    numProcessed = i;
                }
                break;
            }

            countOutputs++;
        }

        // calculate timestamps from the extended counters
        if (output.size() > 0) {
            boolean blockStarted = blockHasJustStarted();
            // detect if output belongs to a new block
            if (blockStarted) {
                // invalid value for minimal offset that shall be needed shortly
                activeBlock.minimalTimestampOffset = Long.MIN_VALUE;
                // timestamp and counter of output.get(0) are not fully fixed yet at this stage, store them later
            }

            calculateTimestampsFromCounters(output);
            /*
            // calculate timestamps from processed counters and detect some known problems
            long startOffsetCounter = output.get(0).counter;
            long startOffsetTimestamp = output.get(0).timestamp;
            if (referenceTimestamp != INVALID_TIMESTAMP) {
                // if referenceTimestamp is given (this collection is a continuation of a block), use that instead
                startOffsetCounter = 0;
                startOffsetTimestamp = referenceTimestamp;
            }
            for (int i = 0; i < output.size(); ++i) {
                long newTs = Math.round(startOffsetTimestamp +
                        (output.get(i).counter - startOffsetCounter) * constantSamplingTime);
                // because reference timestamp is not optimally selected some timestamps will get larger values
                // than possible, this is an error that will get fixed later
                long err = newTs - output.get(i).timestamp;
                if (err > activeBlock.minimalTimestampOffset)
                    activeBlock.minimalTimestampOffset = err;
                output.get(i).timestamp = newTs;
            }
            logger.finer("to convert counters to timestamps, minimal correction needed = "+activeBlock.minimalTimestampOffset/1000000L+" ms ");
            */

            if (blockStarted) {
                // if this is a start of a new block, it has to be aligned in time
                // to build the output, first element in the collection was used as reference, but that is highly
                // likely a bad choice for reference, timeAlignActiveBlock will find a better one from all elements
                // of the collection
                timeAlignActiveBlock(output, cache);
                // this is a new block, fill up the startX variables, now that they are fixed
                activeBlock.startingCounter = output.get(0).getCounter();
                activeBlock.startingTimestamp = output.get(0).getTimestamp();
            }

            // if an end of the block was detected or read from input, then flush this block to archive
            if (blockFinished) {
                logger.finer("finishing a block ["+activeBlock.startingTimestamp/1000000000L + " s - "+
                        output.get(output.size()-1).getTimestamp()/1000000000L+" s]; total num of blocks = " +
                        blocks.size() + "; num processed pairs = " + countOutputs);

                activeBlock.endingCounter = output.get(output.size() - 1).getCounter();
                activeBlock.endingTimestamp = output.get(output.size() - 1).getTimestamp();
                activeBlock.markedStop = markedDisconnect;
                blocks.add(activeBlock);
                // create variable for the new block
                activeBlock = new PcardTimeAlignment.BlockInfo();
            }
        } else {
            logger.finer("Done aligning data on empty interval.");
        }

        // flush the old blocks from internal cache (leaving one in cache)
        for (int i = firstNotFlushedBlock; i < blocks.size()-1; ++i) {
            PcardTimeAlignment.BlockInfo b = blocks.get(i);
            if (!b.flushed) {
                // remove the no longer required block of data fom internal cache
                PcardTimeAlignment.DataCollectionView<TP> toBeFlushed = cache.splice(b);
                toBeFlushed.clear();
                b.flushed = true;
            } else
                break;
        }

        // flush the processed input elements
        input.shiftData(numProcessed);
        return output;
    }

    @Override
    public void finish() {
        if (DEBUG_OUTPUT)
            printoutBlocks();
    }

    /**
     * calculate the difference between counters (non-extended, that is 10-bit)
     *
     * @param cnt1 counter 1
     * @param cnt2 counter 2 (assumed to be 'higher' than counter 1)
     * @return counter difference assuming no disconnects occurred between the counters were received
     */
    private static long getBaseCounterDifference(long cnt1, long cnt2) {
        int ofc1 = get10BitCounterOverflowComponent((int)cnt1);
        int ofc2 = get10BitCounterOverflowComponent((int)cnt2);
        return cnt2 + (overflowComponentsToNumOverflows(ofc1, ofc2) * 1024) - cnt1;
    }

    /**
     * Test whether the data in collection is regular
     * @param data input data
     * @return true if everything seems ok
     */
    private boolean testCounters(PcardTimeAlignment.DataCollection<TP> data) {
        for (int i = 1; i<data.size(); ++i) {
            if (data.get(i).getCounter() <= data.get(i-1).getCounter()) {
                throw new RuntimeException("Problem with 'aligned' data, counters are not monotonically increasing; @"+i);
            }
            if (data.get(i).getTimestamp() <= data.get(i-1).getTimestamp()) {
                throw new RuntimeException("Problem with 'aligned' data, timestamps are not monotonically increasing");
            }
        }
        return true;
    }

    /**
     * Inline convert from counters to timestamps
     * Requirements:
     *  - counters should be extended to full precision
     *  - activeBlock.minimalTimestampOffset must be valid and will be changed
     * @param output data collection to be converted, it is both  input and output
     */
    private void calculateTimestampsFromCounters(PcardTimeAlignment.DataCollection<TP> output) {
        // calculate timestamps from processed counters and detect some known problems
        long startOffsetCounter = output.get(0).getCounter();
        long startOffsetTimestamp = output.get(0).getTimestamp();
        if (!blockHasJustStarted()) {
            // if referenceTimestamp is given (this collection is a continuation of a block), use that instead
            startOffsetCounter = 0;
            startOffsetTimestamp = referenceTimestamp;
        }
        for (int i = 0; i < output.size(); ++i) {
            long newTs = Math.round(startOffsetTimestamp +
                    (output.get(i).getCounter() - startOffsetCounter) * constantSamplingTime);
            // because reference timestamp is not optimally selected some timestamps will get larger values
            // than possible, this is an error that will get fixed later
            long err = newTs - output.get(i).getTimestamp();
            if (err > activeBlock.minimalTimestampOffset)
                activeBlock.minimalTimestampOffset = err;
            output.get(i).setTimestamp(newTs);
        }
        logger.finer("after converting counters to timestamps, minimal timestamp correction needed = "+activeBlock.minimalTimestampOffset/1000000L+" ms ");
    }
    /**
     * Function used to round the number to closest positive and non-zero multiple of counter increment
     * @param counterDiff
     * @return
     */
    private static long roundToPositiveCounterDiff(long counterDiff) {
        return Math.max(1, ((2 * counterDiff) + COUNTER_INCREMENT) / (2*COUNTER_INCREMENT)) * COUNTER_INCREMENT;
    }

    /**
     * After the some data had counters processed, fix their timestamps alignment
     *
     * @param output the output of the first stage of time alignment
     */
    private void timeAlignActiveBlock(PcardTimeAlignment.DataCollection<TP> output, PcardTimeAlignment.DataCollection<TP> cache) {
        // if this is not the first collection after a disconnect/connect, do nothing
        if (referenceTimestamp != PcardTimeAlignment.INVALID_TIMESTAMP)
            return;

        // move timestamps to get a better general time estimate than the first pair's timestamp
        // note that the continuous segments of data (within the same block, i.e. without a
        // disconnect in between) should not be offset in time nor in counters, since they must follow
        // the timestamps linearly, otherwise time holes & compressions would appear
        // Note: cases are admissible where in the continuation of the same block,
        //      a pair will be encountered with negative timestamp error, especially if the frequency
        //      provided to this algorithm is too low

        // fix the block in time dimension
        // 1. this includes the calculated required time offset for this collection - this part makes sure all
        // the timestamps move only backwards in time, and none moves forwards, which is physically impossible
        // 2. the time of the last element of previous block must also be considered in order not to get negative
        // jumps in time between consecutive blocks, which are again not physical (this happens if the frequency
        // value in the algorithm is lower than the actual frequency)
        long timeOffset = activeBlock.minimalTimestampOffset;
        if (blocks.size() > 0) {
            // TODO: this code seems unused (oldBlock.minimalTimestampOffset is always 0?) and wrong!
            /*
            BlockInfo oldBlock = blocks.get(blocks.size() - 1);
            // earliest time of the new block must be higher than latest time of old block
            long minTimeNewBlock = output.get(0).timestamp - activeBlock.minimalTimestampOffset;
            long maxTimeOldBlock = oldBlock.endingTimestamp + (long) ((COUNTER_INCREMENT * 2) * constantSamplingTime);
            logger.finer("oldBlock: " + oldBlock.minimalTimestampOffset / 1000000L + "; newBlock: " + activeBlock.minimalTimestampOffset / 1000000L);
            if (oldBlock.minimalTimestampOffset > timeOffset)
                timeOffset = -oldBlock.minimalTimestampOffset;
                */

            PcardTimeAlignment.BlockInfo oldBlock = blocks.get(blocks.size() - 1);
            long minTimeNewBlock = output.get(0).getTimestamp() - activeBlock.minimalTimestampOffset;
            long maxTimeOldBlock = oldBlock.endingTimestamp + (long) ((COUNTER_INCREMENT) * constantSamplingTime);
            logger.finer("oldBlock: " + maxTimeOldBlock / 1000000L + "; newBlock: " + minTimeNewBlock / 1000000L);
            // do something if newBlock is lower than oldBlock
            // TODO: this actually indicates that the old block is miss-aligned and should be fixed rather than the new block
            if (maxTimeOldBlock > minTimeNewBlock) {
                long tsError = (maxTimeOldBlock - minTimeNewBlock);

                // new procedure - adjust the old block
                long newCounter = estimateCounterFromTimestamp(tsError, 0, 0);
                offsetStoredBlock(cache, blocks.size() - 1, newCounter, tsError);
                /*
                // old procedure - adjust the new block
                activeBlock.minimalTimestampOffset -= error;
                timeOffset = activeBlock.minimalTimestampOffset;
                logger.warning("error in block "+(blocks.size()-1)+", moving next block in time by "+(error / 1000000L)+" ms to hide it");
                */
            }
        }


        logger.finer("newBlock time offset = " + (activeBlock.minimalTimestampOffset / 1000000L) + " ms");
        for (int io = 0; io < output.size(); ++io)
            output.get(io).setTimestamp(output.get(io).getTimestamp() - timeOffset);

        // if alignment error is large (in seconds), it indicates that there was large compensation on
        // counters (possibly in error); therefore these counters should be modified back to their old values
        // convert maxTimestamp error to counter dimension and round it to nearest 14

        // pause13 detected (possibly other stuff as well)
        // what to do - try to detect if the block boundary got accidentally stretched in counters and
        // if so, fix those counters
        // methodology:
        //  - assume timestamps within the block are ok
        //  - expect a pause between blocks and then again somewhere near the beginning of the new block
        //      * between blocks, pause is created because timestamps differ enough to consider it unmarked disconnect
        //      * in the beginning of block, some old pairs (that have timestamp wildly off) are placed, but
        //          pause is not detected, since their timestamps increase by small (seemingly normal) amounts
        //  - if such pause exists, modify counters (fix them back, since they were broken in some previous step
        // this if used to be:
        //          if (true || activeBlock.minimalTimestampOffset > MIN_INTERVAL_FOR_UNMARKED_DISCONNECT) {
        // but it should be executed always when counters were modified, which is now stored in initialCounterOffset field of active block
        if (activeBlock.initialCounterOffset > 0) {
            // check that block that just ended comes after an unmarked disconnect
            if (blocks.size() > 0) {
                long counterError = estimateCounterFromTimestamp(activeBlock.minimalTimestampOffset, 0, 0);
                // going from the other end - see where are the limits for counter (to avoid overlapping)
                PcardTimeAlignment.BlockInfo prevBlock = blocks.get(blocks.size() - 1);
                long minCounterForTimestamp = prevBlock.endingCounter;
                // the above defined minimum would make it literally the next pair; TODO: take timestamps into account as well...

                // rounded counter error is the best estimate of what the values would be if they were not compensated
                // for a perceived unmarked disconnect; TODO: if these were stored somewhere, it would ease these calculations
                //
                // estimated new counter of the pair in collection at index 0
                long estimatedNewCounter0 = output.get(0).getCounter() - counterError;
                // make the difference between last counter of old block and first counter of new block a multiple
                // of 14, and larger or equal to one increment
                long roundedCounterDiff = roundToPositiveCounterDiff(estimatedNewCounter0 - minCounterForTimestamp);
                estimatedNewCounter0 = minCounterForTimestamp + roundedCounterDiff;
                long roundedCounterError = output.get(0).getCounter() - estimatedNewCounter0;

                logger.log(Level.FINE, "offsetting counters by " + (-roundedCounterError) + " (estimate was " +
                        (-counterError) + "), inter-block counter increment is " +
                        (estimatedNewCounter0 - minCounterForTimestamp) + " (" +
                        (estimatedNewCounter0 - minCounterForTimestamp) * (1e-9) * constantSamplingTime + " s)" +
                        " previous block time error = " + blocks.get(blocks.size() - 1).minimalTimestampOffset / 1000000L + " ms"
                );

                {   // DEBUGGING only: comment out for release
                    //roundedCounterError = counterError;
                }
                for (int io = 0; io < output.size(); ++io)
                    output.get(io).setCounter(output.get(io).getCounter() - roundedCounterError);
                prevExtendedCounter -= roundedCounterError;
                counterOffset -= roundedCounterError;
                activeBlock.minimalTimestampOffset -= timeOffset;
                timeOffset = 0;

                // note that rounding counters to nearest COUNTER_INCREASE can again produce errors in timestamps ...
                //referenceTimestamp = INVALID_TIMESTAMP;
                calculateTimestampsFromCounters(output);
            }
        } else {
        }

        // the following collections that are part of the same block will have to be aligned to the same reference time
        referenceTimestamp = offsetTimestampByCounterValue(output.get(0).getTimestamp(), -output.get(0).getCounter());
        logger.fine("resetting reference timestamp to " + referenceTimestamp + "; num pairs used to calculate it = " + output.size());
        //activeBlock.minimalTimestampOffset = 0;
        activeBlock.minimalTimestampOffset -= timeOffset;

        logger.log(Level.FINE, " time aligned block: initial time="+output.get(0).getTimestamp()*1e-9+"; timeoffset="+(timeOffset*1e-9)+" s");
    }

    /**
     * Estimate the counter at given timestamp and given reference pair of known corresponding timestamp and counter
     * @param timestamp      target timestamp
     * @param firstTimestamp reference timestamp
     * @param firstCounter   reference counter
     * @return the counter at given timestamp
     */
    private long estimateCounterFromTimestamp(long timestamp, long firstTimestamp, long firstCounter) {
        return firstCounter + Math.round((timestamp - firstTimestamp) * constantFs);
    }

    /**
     * Convert two overflow components to the number of overflows that occurred between them
     * @param ovfPrior overflow component of first counter
     * @param ovfAfter overflow component of second counter
     * @return number of overflows that occurred between counters
     */
    private static int overflowComponentsToNumOverflows(int ovfPrior, int ovfAfter) {
        return (ovfAfter - ovfPrior+7)%7;
    }

    /**
     * inline counter expansion
     * Note: assumes that the pair.counter is a 10-bit counter
     *
     * The method:
     *  1. Fix for overflow from previous counter value using 'overflow component' (1) of this and previous counter.
     *      This overflow calculation is accurate up to sample difference of 8*1024 (64 s @ 128 Hz, 32 s @ 256 Hz),
     *      which is enough to make it impervious to everything but a disconnect
     *  2. Fix for disconnects (only if they are explicitly registered!)
     *  3. Output is always a valid counter (larger than previous counter)
     *
     *  (1) overflow component is the number that can be extracted from 10-bit counter which increases by 14:
     *      first time around (no overflows yet) it will increment 0, 14, 28, ...
     *      second time around (one overflow) it will increment 12, 26, ...
     *      third time around (two overflows) it will increment 10, 24, ...
     *      and so on until it will come back to 0, 14, 28 after 7 overflows
     * @param pair the data to work on (fix counter value in-place)
     * @return true if no problems were encountered, false if the pair is invalid (usually because of invalid counter)
     */
    public boolean processPacketCounter(TimestampPair pair) {
        // variable for storing extended (overflow corrected) counter from pair.counter
        long extendedCounter = PcardTimeAlignment.INVALID_COUNTER;
        // make this false by default, since is is the most frequent
        boolean expectFirstPacket_nextValue = false;

        if (pair.getCounter() == PcardTimeAlignment.INVALID_COUNTER) {
            // invalid counters (which marks a disconnect) was detected
            expectFirstPacket_nextValue = true;
            counterOffset = 0;
            logger.log(Level.FINE, "marked disconnect at approx "+pair.getTimestamp()/1000000000L + " s; last timestamp = "+prevTimestamp/1000000000L+" s");
        } else {
            // normal counter was detected (not a disconnect mark)
            // fix the counter
            int counterOvf = get10BitCounterOverflowComponent((int) pair.getCounter());
            int numCounterOverflows = overflowComponentsToNumOverflows(prevCounterOvf, counterOvf);

            // fix for known disconnects and dealing with the very first element
            if (prevCounter == PcardTimeAlignment.INVALID_COUNTER) {
                // the very first packet is processed here (the only option to have previous counter invalid)
                counterOffset = 0;
                extendedCounter = pair.getCounter();
                logger.finer("starting the first block ["+pair.getTimestamp()/1000000000L+" s - ?]");
            } else {
                // is first packet of a block expected?
                if (expectFirstPacket) {
                    // this can only be the first datum after a disconnect/connect since the very first packet was
                    // handled above; handle this by invalidating and recalculating the counter offset

                    logger.finer("starting block ["+pair.getTimestamp()/1000000000L+" s - ?] number "+blocks.size());
                    // reference timestamp is best reset at this time
                    referenceTimestamp = PcardTimeAlignment.INVALID_TIMESTAMP;

                    // try fixing counters with the reference timestamps (the end of last block)
                    long absoluteCounter = estimateCounterFromTimestamp(pair.getTimestamp(), prevTimestamp, prevExtendedCounter);

                    counterOffset = absoluteCounter - pair.getCounter();

                    if (counterOffset < 1e-9) {
                        logger.warning("Invalid counter offset calculated - this is a bug in time align algorithm");
                    }

                    extendedCounter = absoluteCounter;
                    activeBlock.initialCounterOffset = (extendedCounter-prevExtendedCounter);

                    logger.log(Level.FINER, "start of new block: setting counter to "+extendedCounter + " from = "+prevExtendedCounter +
                            " (that is an increase of "+(extendedCounter-prevExtendedCounter)+")");
                } else {
                    counterOffset += 1024 * numCounterOverflows;
                    extendedCounter = pair.getCounter() + counterOffset;

                    // Note that numCounterOverflows might be 0, but previous extended counter still
                    // larger than or equal than the new extended counter
                    if (extendedCounter <= prevExtendedCounter) {
                        logger.fine("timestamp "+(pair.getTimestamp()/1000000000L)+
                                "s marks a *reconnect* with pause length = " + (pair.getTimestamp() - prevTimestamp)/1000000 +
                                "ms with counter value falling by " + (prevExtendedCounter - extendedCounter) +
                                " -> assuming an unmarked disconnect."
                        );

                        // mimic the behaviour of reading an invalid counter (marked disconnect)
                        expectFirstPacket_nextValue = true;

                        // TODO: is it safe to leave extended counter in this shape?
                    }
                }
            }

            if (expectFirstPacket) {
                // if this is the first packet then the next one is no longer first
                expectFirstPacket_nextValue = false;
            } else {
                // fix for unknown disconnects
                // Note: can be overly liberal with the min interval, since it should not matter much if
                //  normal operation with multiple lost packets is marked a disconnect
                if ((prevTimestamp != PcardTimeAlignment.INVALID_TIMESTAMP) && ((pair.getTimestamp() - prevTimestamp) > MIN_INTERVAL_FOR_UNMARKED_DISCONNECT)) {
                    logger.log(Level.FINE, "timestamp "+(pair.getTimestamp()/1000000000L)+
                            "s marks the end of *long* pause with length = " + (pair.getTimestamp() - prevTimestamp)/1000000 +
                            "ms (> " + MIN_INTERVAL_FOR_UNMARKED_DISCONNECT /1000000 +
                            "ms) -> assuming an unmarked disconnect."
                    );
                    // mimic the behaviour of reading an invalid counter (marked disconnect)
                    expectFirstPacket_nextValue = true;
                }
            }

            // conditionally prepare for the next iteration
            if (!expectFirstPacket_nextValue) {
                prevCounter = pair.getCounter();
                prevCounterOvf = counterOvf;
                prevExtendedCounter = extendedCounter;
                prevTimestamp = pair.getTimestamp();
                pair.setCounter(extendedCounter);
            } else {
                // this pair will be processed again, so leave prev* values as they are
            }
        }

        // setup for the next iteration
        expectFirstPacket = expectFirstPacket_nextValue;
        return !expectFirstPacket;
    }

    /**
     * Offset a block of data that has been already processes
     * @param collection    The collection to perform offset on
     * @param counterOffset offset in counters of all datum points of the block (to decrease counters provide negative offset)
     * @param timeOffset    offset in timestamps of all datum points of the block (to decrease counters provide negative offset)
     */
    private void offsetStoredBlock(PcardTimeAlignment.DataCollection<TP> collection, int blockIndex, long counterOffset, long timeOffset) {
        // search for the block, going backwards from the end of collection
        if (blockIndex < 0)
            throw new RuntimeException("invalid index (< 0) given to offsetStoredBlock");
        if (blockIndex >= blocks.size())
            throw new RuntimeException("invalid index (too large) given to offsetStoredBlock");
        PcardTimeAlignment.BlockInfo b = blocks.get(blockIndex);

        PcardTimeAlignment.DataCollectionView<TP> temp = collection.splice(b);
        if (temp.size() > 0) {
            for (TP d : temp) {
                d.setCounter(d.getCounter() + counterOffset);
                d.setTimestamp(d.getTimestamp() + timeOffset);
            }
        }
    }

    /**
     * Debugging facility
     */
    public void printoutBlocks() {
        System.out.println("Blocks ("+blocks.size()+"): ");
        for (int i = 0; i<blocks.size(); ++i) {
            System.out.print("  "+blocks.get(i).startingCounter+" -> "+blocks.get(i).endingCounter);
            System.out.print("  "+blocks.get(i).startingTimestamp/1000000000L+"s -> "+
                    blocks.get(i).endingTimestamp/1000000000L+"s");
            System.out.println("  "+(blocks.get(i).markedStop ? "marked" : "unmarked"));
        }
    }

    /**
     * Reset the algorithm so it can be restarted (fill all variables with initial values)
     */
    public void reset() {
        counterOffset = 0;
        prevCounter = PcardTimeAlignment.INVALID_COUNTER;
        prevCounterOvf = 0;
        prevExtendedCounter = 0;
        prevTimestamp = PcardTimeAlignment.INVALID_TIMESTAMP;
        expectFirstPacket = true;
        constantFs = -1.0;
        constantSamplingTime = -1.0;

        referenceTimestamp = PcardTimeAlignment.INVALID_TIMESTAMP;
        countOutputs = 0;
        blocks.clear();;
        activeBlock = new PcardTimeAlignment.BlockInfo();
        firstNotFlushedBlock = 0;
        silentDisconnectDetection = true;
    }
}
